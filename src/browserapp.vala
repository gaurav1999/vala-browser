public class BrowserApp : Gtk.Application {

  public BrowserApp () {
    Object (application_id: "org.gnome.browserexample",
            flags : ApplicationFlags.FLAGS_NONE);
  }

  public override void activate () {
    new Window (this).show_all ();
  }

}

